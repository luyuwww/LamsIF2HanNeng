package com.landray.kmss.sys.notify.webservice;

import javax.xml.rpc.ServiceException;

public class TestToDo {
    public static void main(String[] args) throws  ServiceException, Exception_Exception  {
        ISysNotifyTodoWebServiceService ser = new ISysNotifyTodoWebServiceService();
        NotifyTodoSendContext context  = new NotifyTodoSendContext();
        context.setAppName("紫光档案管理系统");
        context.setModelName("赋权审批");
        context.setModelId("act4tmpright");
        
        context.setSubject("Lams 测试待办webservice~~~");
        context.setLink("http://news.sina.com.cn/");
        context.setType(1);
        context.setKey("这是一个test,来自档案系统,可以忽略");
        context.setTargets("{\"LoginName\":\"wangxing\"}");
        context.setCreateTime("2014-07-17 09:25:09");
//        
        NotifyTodoAppResult result = ser.getISysNotifyTodoWebServicePort().sendTodo(context);
        System.out.println(result.getReturnState());
        System.out.println(result.getMessage());
        
//        NotifyTodoRemoveContext context = new NotifyTodoRemoveContext();
//        context.setAppName("紫光档案管理系统");
//        context.setModelName("赋权审批");
//        context.setModelId("act4tmpright");//这是taskid
//        
//        context.setOptType(1);
//        context.setKey("这是一个test,来自档案系统,可以忽略");
//        context.setTargets("{\"LoginName\":\"ADMIN\"}");
//        
//        NotifyTodoAppResult result = ser.getISysNotifyTodoWebServicePort().setTodoDone(context);
//        System.out.println(result.getReturnState());
//        System.out.println(result.getMessage());
        
        
        //删除
//        NotifyTodoAppResult result = ser.getISysNotifyTodoWebServicePort().deleteTodo(context);
//      System.out.println(result.getReturnState());
//      System.out.println(result.getMessage());
    }
}
