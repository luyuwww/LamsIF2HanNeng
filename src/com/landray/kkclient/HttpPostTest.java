package com.landray.kkclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class HttpPostTest {
    void testPost(String urlStr) {
        try {
            String xmlInfo = getXmlInfo();
            URL url = new URL(urlStr);
            URLConnection con = url.openConnection();
            con.setDoOutput(true);
            con.setRequestProperty("Pragma:", "no-cache");
            con.setRequestProperty("Cache-Control", "no-cache");
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            OutputStreamWriter out = new OutputStreamWriter(con
                    .getOutputStream());    
           
            out.write(xmlInfo);
            out.flush();
            out.close();
            BufferedReader br = new BufferedReader(new InputStreamReader(con
                    .getInputStream()));
            String line = "";
            for (line = br.readLine(); line != null; line = br.readLine()) {
                System.out.println(line);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getXmlInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        sb.append("<request  id=\"c9f29aaad41ce873189f9487a119b0c9\" user=\"\" password=\"\" >");
        sb.append("<message type=\"smart-msg\" cmd=\"send-im-msg\">");
        sb.append("<parameter from-account=\"wangxing\" from-name =\"王星\" to-account=\"wangna01\"  to-name=\"王娜\" important-level=\"high\" emergent-level=\"high\">");
        sb.append(" <text>你好！一条来自业务网关的消息</text>");
        sb.append(" <url>http://www.baidu.com</url>");
        sb.append(" </parameter>");
        sb.append(" </message>");
        sb.append(" </request>");
        System.out.println(sb.toString());
        return sb.toString();
    }

    public static void main(String[] args) {
        String url = "http://kk.hanergy.com:8082";
        new HttpPostTest().testPost(url);
    }
}
