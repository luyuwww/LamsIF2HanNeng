package com.landray.kkclient;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class Test2 {
    public static void main(String[] args) {
        String xmlInfo = getXmlInfo();
        HttpURLConnection http;

        try {
            URL urls = new URL("http://kk.hanergy.com:5200");
            http = (HttpURLConnection) urls.openConnection();
            http.setDoOutput(true);
            http.setDoInput(true);
            http.setRequestMethod("POST");
            OutputStreamWriter out = new OutputStreamWriter(http.getOutputStream());
            out.write(xmlInfo);
            out.flush();
            out.close();

            // System.out.println( in(http.getInputStream()));
            // 接口返回的报文

            String resp = "";
            java.io.BufferedReader breader =
                    new BufferedReader(new InputStreamReader(http.getInputStream(), "UTF-8"));
            String str = breader.readLine();
            while (str != null) {
                resp += str;
                str = breader.readLine();
            }
            System.out.println("resp=" + resp);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static String getXmlInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        sb.append("<request  id=\"c9f29aaad41ce873189f9487a119b0c9\" user=\"\" password=\"\" >");
        sb.append("<message type=\"smart-msg\" cmd=\"send-im-msg\">");
        sb.append("<parameter from-account=\"wangxing\" from-name =\"王星\" to-account=\"wangna01\"  to-name=\"王娜\" important-level=\"high\" emergent-level=\"high\">");
        sb.append(" <text>你好！一条来自业务网关的消息</text>");
        sb.append(" <url>http://www.baidu.com</url>");
        sb.append(" </parameter>");
        sb.append(" </message>");
        sb.append(" </request>");
        System.out.println(sb.toString());
        return sb.toString();
    }

}
