package com.thams.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.thams.entity.OAEfile;
import com.thams.entity.OAEfileExample;

public interface OAEfileMapper {

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table OA_DATA_EFILE
	 * @mbggenerated
	 */
	int countByExample(OAEfileExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table OA_DATA_EFILE
	 * @mbggenerated
	 */
	int deleteByExample(OAEfileExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table OA_DATA_EFILE
	 * @mbggenerated
	 */
	int deleteByPrimaryKey(Integer did);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table OA_DATA_EFILE
	 * @mbggenerated
	 */
	int insert(OAEfile record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table OA_DATA_EFILE
	 * @mbggenerated
	 */
	int insertSelective(OAEfile record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table OA_DATA_EFILE
	 * @mbggenerated
	 */
	List<OAEfile> selectByExampleWithBLOBs(OAEfileExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table OA_DATA_EFILE
	 * @mbggenerated
	 */
	List<OAEfile> selectByExample(OAEfileExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table OA_DATA_EFILE
	 * @mbggenerated
	 */
	OAEfile selectByPrimaryKey(Integer did);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table OA_DATA_EFILE
	 * @mbggenerated
	 */
	int updateByExampleSelective(@Param("record") OAEfile record,
			@Param("example") OAEfileExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table OA_DATA_EFILE
	 * @mbggenerated
	 */
	int updateByExampleWithBLOBs(@Param("record") OAEfile record,
			@Param("example") OAEfileExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table OA_DATA_EFILE
	 * @mbggenerated
	 */
	int updateByExample(@Param("record") OAEfile record,
			@Param("example") OAEfileExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table OA_DATA_EFILE
	 * @mbggenerated
	 */
	int updateByPrimaryKeySelective(OAEfile record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table OA_DATA_EFILE
	 * @mbggenerated
	 */
	int updateByPrimaryKeyWithBLOBs(OAEfile record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table OA_DATA_EFILE
	 * @mbggenerated
	 */
	int updateByPrimaryKey(OAEfile record);
}