package com.thams.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.thams.entity.SQzh;
import com.thams.entity.SQzhExample;

public interface SQzhMapper {

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table S_QZH
	 * @mbggenerated
	 */
	int countByExample(SQzhExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table S_QZH
	 * @mbggenerated
	 */
	int deleteByExample(SQzhExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table S_QZH
	 * @mbggenerated
	 */
	int deleteByPrimaryKey(Integer did);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table S_QZH
	 * @mbggenerated
	 */
	int insert(SQzh record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table S_QZH
	 * @mbggenerated
	 */
	int insertSelective(SQzh record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table S_QZH
	 * @mbggenerated
	 */
	List<SQzh> selectByExample(SQzhExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table S_QZH
	 * @mbggenerated
	 */
	SQzh selectByPrimaryKey(Integer did);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table S_QZH
	 * @mbggenerated
	 */
	int updateByExampleSelective(@Param("record") SQzh record,
			@Param("example") SQzhExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table S_QZH
	 * @mbggenerated
	 */
	int updateByExample(@Param("record") SQzh record,
			@Param("example") SQzhExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table S_QZH
	 * @mbggenerated
	 */
	int updateByPrimaryKeySelective(SQzh record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table S_QZH
	 * @mbggenerated
	 */
	int updateByPrimaryKey(SQzh record);
	
	@Select("SELECT QZH FROM S_QZH WHERE QZMC='${qzmc}'")
	String getQzhByQzmc(@Param("qzmc") String qzmc);
}