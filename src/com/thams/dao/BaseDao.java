package com.thams.dao;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.log4j.Logger;

import com.thams.core.util.Utils;
import com.thams.entity.EFile;
import com.thams.entity.OAEfile;
import com.thams.entity.OAEfileExample;
import com.thams.entity.OAGZLXDWqt3;
import com.thams.entity.OAGZLXDWqt3Example;
import com.thams.entity.QAHTWqt2;
import com.thams.entity.QAHTWqt2Example;
import com.thams.entity.QAWqt1;
import com.thams.entity.QAWqt1Example;
import com.thams.entity.Wqt1;
import com.thams.entity.Wqt2;
import com.thams.entity.Wqt3;

public class BaseDao {
	private static SqlSessionFactory sqlSessionFactory = null;  
	private static Logger log = Logger.getLogger(BaseDao.class);
	public static String OA_HT_PATH = "";
	public static String OA_SFW_PATH = "";
	public static String OA_GZLXD_PATH = "";
	public static String DEFAULT_QZH = "";
	public static String DEFAULT_PZM = "";//默认配置名
	public static String DEFAULT_PATH = "";//默认ftp磁盘目录
	public static String SEND_MSG_VM = "";//msg模板
	public static String LAMS_IP = "";//lams的ip
	public static String TODO_URL = "";//todo 的 url
    static {  
        try {  
        	InputStream is = BaseDao.class.getClassLoader().getResourceAsStream("config"+File.separator+"mybatis.xml");
        	Properties prop = new Properties();
        	prop.load(BaseDao.class.getClassLoader().getResourceAsStream("config"+File.separator+"config.properties"));
        	OA_HT_PATH = prop.getProperty("OA_HT_PATH");
        	OA_SFW_PATH = prop.getProperty("OA_SFW_PATH");
        	OA_GZLXD_PATH = prop.getProperty("OA_GZLXD_PATH");
        	DEFAULT_QZH = prop.getProperty("DEFAULT_QZH");
        	DEFAULT_PZM = prop.getProperty("DEFAULT_PZM");
        	DEFAULT_PATH = prop.getProperty("DEFAULT_PATH");
        	SEND_MSG_VM = prop.getProperty("SEND_MSG_VM");
        	LAMS_IP = prop.getProperty("LAMS_IP");
        	TODO_URL = prop.getProperty("TODO_URL");
    		sqlSessionFactory = new SqlSessionFactoryBuilder().build(is,"development");
    		File logBasePaht = new File("c:/syncUserGroupLog");
    		if(!logBasePaht.isDirectory()){
    			logBasePaht.mkdir();
    		}
        } catch (Exception e) {  
            e.printStackTrace();  
            log.error("初始化MyBatis错误", e);
        }  
    }
      
    public static SqlSessionFactory getSqlSessionFactory() {  
        return sqlSessionFactory;  
    }
    
    public static SqlSession getSqlSession(){
    	return sqlSessionFactory.openSession();
    }
    public static SqlSession getSqlSession(Boolean isAutoCommit){
    	return sqlSessionFactory.openSession(isAutoCommit);
    }
    
    /**
     * 得到表内最大did 已经+1了
     * @param tableName 
     */
    public synchronized static int getMaxDid(String tableName){
    	Integer maxdid = 1;
    	SqlSession session = null;
    	SUserMapper sUserMapper = null;
		try {
			session = BaseDao.getSqlSession();
			sUserMapper = session.getMapper(SUserMapper.class);
			maxdid = sUserMapper.getMaxDid(tableName);
			if(maxdid == null || maxdid ==0){
				maxdid = 1;
			}else{
				maxdid = maxdid +1;
			}
		}catch (Exception e) {
			maxdid = -1;
		}finally{
			session.close();
		}
		return maxdid;
    }
    /**
     * 得到表内最大did 已经+1了
     * @param tableName
     */
    public static String getUserNameByCode(String userName){
    	String userCode = "";
    	SqlSession session = null;
    	SUserMapper sUserMapper = null;
		try {
			session = BaseDao.getSqlSession();
			sUserMapper = session.getMapper(SUserMapper.class);
			userCode = sUserMapper.getUserCodeByName(userName);
			if(StringUtils.isEmpty(userCode)){
				userCode = "";
			}
		}catch (Exception e) {
			userCode = "";
			log.error("通过用户中文名得到用户code错误"+userName , e);
		}finally{
			session.close();
		}
		return userCode;
    }
    
    /**
     * 插入电子文件
     * DID,PID,EFILENAME,TITLE,EXT,PZM,PATHNAME,EFILEDB,EFILEID,STATUS,ATTR,ATTREX,CREATOR,CREATETIME,MD5
     */
    public static void insertEfile(EFile eFile){
		SqlSession session = null;
		try {
			session = BaseDao.getSqlSession(true);
			session.insert("com.thams.dao.OAEfileMapper.insertEfile", eFile);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
	}
    
    /**
     * 通过pid得到所有的Efile
     * @param pid
     */
    public static List<OAEfile> getOaEfileListByPid(String pid){
    	SqlSession session = null;
    	OAEfileMapper efileMap = null;
    	OAEfileExample oaEEx = new OAEfileExample();
    	oaEEx.createCriteria().andPidEqualTo(pid);
    	List<OAEfile> resultList = null;
		try {
			session = BaseDao.getSqlSession(true);
			efileMap = session.getMapper(OAEfileMapper.class);
			resultList = efileMap.selectByExample(oaEEx);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return resultList;
    }
    
    /**
     * 插入Wqt11
     */
    public static Boolean insertWQT1(Wqt1 wqt1){
    	SqlSession session = null;
    	Wqt1Mapper wm = null;
    	Boolean result = false;
		try {
			session = BaseDao.getSqlSession(true);
			wm = session.getMapper(Wqt1Mapper.class);
			wm.insert(wqt1);
		} catch (Exception e) {
			result = false;
		}finally{
			session.close();
		}
		return result;
    }
    /**
     * 插入Wqt2
     */
    public static Boolean insertWQT2(Wqt2 qt2){
    	SqlSession session = null;
    	Wqt2Mapper wm = null;
    	Boolean result = false;
    	try {
    		session = BaseDao.getSqlSession(true);
    		wm = session.getMapper(Wqt2Mapper.class);
    		wm.insert(qt2);
    	} catch (Exception e) {
    		result = false;
    	}finally{
    		session.close();
    	}
    	return result;
    }
    /**
     * 插入Wqt3
     */
    public static Boolean insertWQT3(Wqt3 qt3){
    	SqlSession session = null;
    	Wqt3Mapper wm = null;
    	Boolean result = false;
    	try {
    		session = BaseDao.getSqlSession(true);
    		wm = session.getMapper(Wqt3Mapper.class);
    		wm.insert(qt3);
    	} catch (Exception e) {
    		result = false;
    	}finally{
    		session.close();
    	}
    	return result;
    }
    
	/**
	 * status 0 表示档案系统没有接受,1.表示接受完毕.2接受错误
	 */
    public static List<QAWqt1> getAllListQaWqt1ListByStatus(Integer status){
		SqlSession session = null;
		QAWqt1Mapper oaWqt1Maper = null;
		QAWqt1Example oaWqt1Ex = new QAWqt1Example();
		oaWqt1Ex.createCriteria().andStatusEqualTo(status);
		List<QAWqt1> result = null;
		try {
			session = BaseDao.getSqlSession();
			oaWqt1Maper = session.getMapper(QAWqt1Mapper.class);
			result = oaWqt1Maper.selectByExample(oaWqt1Ex);
		} catch (Exception e) {
			log.error("出现异常" , e);
		}finally{
			session.close();
		}
		return result;
	}
	/**
	 * status 0 表示档案系统没有接受,1.表示接受完毕.2接受错误
	 */
	public static List<QAHTWqt2> getAllListQaWqt2ListByStatus(Integer status){
		SqlSession session = null;
		QAHTWqt2Mapper htMaper = null;
		QAHTWqt2Example ex= new QAHTWqt2Example();
		ex.createCriteria().andStatusEqualTo(status);
		List<QAHTWqt2> result = null;
		try {
			session = BaseDao.getSqlSession();
			htMaper = session.getMapper(QAHTWqt2Mapper.class);
			result = htMaper.selectByExample(ex);
		} catch (Exception e) {
			log.error("出现异常" , e);
		}finally{
			session.close();
		}
		return result;
	}
	
	/**
	 * status 0 表示档案系统没有接受,1.表示接受完毕.2接受错误
	 */
	public static List<OAGZLXDWqt3> getAllListQaGzLxListByStatus(Integer status){
		SqlSession session = null;
		OAGZLXDWqt3Mapper htMaper = null;
		OAGZLXDWqt3Example ex= new OAGZLXDWqt3Example();
		ex.createCriteria().andStatusEqualTo(status);
		List<OAGZLXDWqt3> result = null;
		try {
			session = BaseDao.getSqlSession();
			htMaper = session.getMapper(OAGZLXDWqt3Mapper.class);
			result = htMaper.selectByExample(ex);
		} catch (Exception e) {
			log.error("出现异常" , e);
		}finally{
			session.close();
		}
		return result;
	}
	public static void updateStatus(String tableName , String did , Integer status){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("tableName", tableName);
		params.put("did", did);
		params.put("status", status);
		SqlSession session = null;
		try {
			session = BaseDao.getSqlSession(true);
			session.update("com.thams.dao.OAEfileMapper.updateStats", params);
		} catch (Exception e) {
			log.error("出现异常" , e);
		}finally{
			session.close();
		}
	}
	
	/**
	 * 和OA约定 汉能控股集团$1 传入的 起草部门为这个格式.
	 */
	public static String getBMIDByHanergy(String str){
		String result = "";
		if(StringUtils.isEmpty(str)){
			result = BaseDao.DEFAULT_QZH;
		}else{
			try {
				String qzmc = str.split("[$]")[0];
				Integer groupDid = Integer.parseInt(str.split("[$]")[1]);
				result = new Utils().getBMIDByGroupDid(groupDid);
				if(StringUtils.isEmpty(result)){
					SqlSession session = null;
					try {
						session = BaseDao.getSqlSession();
						String tempStr = session.getMapper(SQzhMapper.class).getQzhByQzmc(qzmc);
						result = StringUtils.isEmpty(tempStr) ? BaseDao.DEFAULT_QZH : tempStr;
					} catch (Exception e) {
						log.error("出现异常" , e);
					}finally{
						session.close();
					}
				}
			} catch (Exception e) {
				result = BaseDao.DEFAULT_QZH;
			}
		}
		return result;
				
	}
}
