package com.thams.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.thams.core.util.Utils;
import com.thams.service.DataOperService;

/**
 * Servlet implementation class SyncData
 */
public class SyncDataServlet extends HttpServlet {

	void dolist(HttpServletRequest request, HttpServletResponse response) {
		try {
			String command = request.getParameter("op");
			int i = 0;
			for (i = 0; i < cmdlist.length; i++) {
				if (cmdlist[i].equals(command)) {
					break;
				}
			}
			switch (i) {
			case 0:
				starSync(request, response);
				break;
			default:
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	// 开始同步
	public void starSync(HttpServletRequest request,
			HttpServletResponse response) {
		try {
			DataOperService us = new DataOperService();
			us.starAll();
			Utils.outPrint(response, "已经同步完成", false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 查询节约信息结果
	public void BorrowResult(HttpServletRequest request,
			HttpServletResponse response) {
		try {
		} catch (Exception e) {
		}
	}

	protected void service(HttpServletRequest arg0, HttpServletResponse arg1)
			throws ServletException, IOException {
		dolist(arg0, arg1);
	}

	private String cmdlist[] = { "starSync"};
	private static final long serialVersionUID = 2621977516956958710L;
}
