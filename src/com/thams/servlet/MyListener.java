package com.thams.servlet;

import java.util.Timer;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class MyListener implements ServletContextListener {

	private Timer timer = null;

	public void contextInitialized(ServletContextEvent event) {
		Long seconds = Long.parseLong(event.getServletContext().getInitParameter("syncTimes"));
		timer = new Timer(true);
		// 设置任务计划，启动和间隔时间
		timer.schedule(new MyTask(), 0, seconds);//设置4000毫秒
	}

	public void contextDestroyed(ServletContextEvent event) {
		timer.cancel();
	}
	public static void main(String args[]){
		MyListener ml = new MyListener();
		new MyTask().run();
	}
}