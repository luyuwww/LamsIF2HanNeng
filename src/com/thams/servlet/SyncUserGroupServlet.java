package com.thams.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

import com.landray.kmss.sys.notify.webservice.ISysNotifyTodoWebServiceService;
import com.landray.kmss.sys.notify.webservice.NotifyTodoAppResult;
import com.landray.kmss.sys.notify.webservice.NotifyTodoRemoveContext;
import com.landray.kmss.sys.notify.webservice.NotifyTodoSendContext;
import com.thams.core.util.SeriKeyOper;
import com.thams.core.util.Utils;
import com.thams.dao.BaseDao;
import com.thams.entity.SGroup;
import com.thams.entity.SUser;
import com.thams.service.UserGroupService;

/**
 * Servlet implementation class SyncUserGroup
 */
public class SyncUserGroupServlet extends HttpServlet {

	void dolist(HttpServletRequest request, HttpServletResponse response) {

		try {
			String command = request.getParameter("op");
			int i = 0;
			for (i = 0; i < cmdlist.length; i++) {
				if (cmdlist[i].equals(command)) {
					break;
				}
			}
			switch (i) {
			case 0:
				listUser(request, response);
				break;
			case 1:
				listGroup(request, response);
				break;
			case 2:
				starSync(request, response);
				break;
			case 3:
				sendMsg(request, response);
				break;
			case 4:
				viewLogList(request, response);
				break;
			case 5:
				viewLog(request, response);
				break;
			case 6:
				sendmsg4Lams(request, response);
				break;
				// operData(request, response);
			case 7:
			    sendDestoryMsg(request, response);
			    break;
			default:
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	// 查询节约信息结果
	public void listUser(HttpServletRequest request,
			HttpServletResponse response) {
		try {
			UserGroupService us = new UserGroupService();
			List<SUser> userList = us.getAllUser();
			request.setAttribute("allUser", userList);
			Utils.callJSP(request, response, "/page/listUser.jsp");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 查询节约信息结果
	public void listGroup(HttpServletRequest request,
			HttpServletResponse response) {
		try {
			UserGroupService us = new UserGroupService();
			List<SGroup> groupList = us.getAllGroup();
			request.setAttribute("groupList", groupList);
			Utils.callJSP(request, response, "/page/listGroup.jsp");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 开始同步
	public void starSync(HttpServletRequest request, HttpServletResponse response) {
		try {
			UserGroupService us = new UserGroupService();
			us.starSyncUserGroup();
			Utils.outPrint(response, "已经同步完成", false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void sendMsg(HttpServletRequest request, HttpServletResponse response) {
	    try {
            String goComMsgTitle = getServletContext().getInitParameter("GoComMsgTitle");
            String userCodes = request.getParameter("userCodes");
            String varsJson = request.getParameter("varsJson");
            String actTaskID = request.getParameter("actTaskID");

            String sqrbm = "";
            String sqrxm = "";
            String sqyy = "";
            String mj = "";
            SUser user = null;
            if(StringUtils.isNotEmpty(varsJson) && StringUtils.isNotEmpty(userCodes)){
                ObjectMapper mapper = new ObjectMapper();  
                Map<String,Object> vars = null;
                try {
                    String[] userCodeList = StringUtils.split(userCodes,"[,]");
                    vars = mapper.readValue(varsJson, Map.class); 
                    sqrxm = (vars.get("sqrxm") == null ? "" : vars.get("sqrxm").toString());
                    sqrbm = (vars.get("sqrbm") == null ? "" : vars.get("sqrbm").toString());
                    sqyy = (vars.get("sqyy") == null ? "" : vars.get("sqyy").toString());
                    mj = (vars.get("mj") == null ? "" : vars.get("mj").toString());
                    String content = BaseDao.SEND_MSG_VM.replace("@usergroupname", sqrbm);
                    content = content.replace("@username", sqrxm);
                    content = content.replace("@sqyy", sqyy);
                    content = content.replace("@mj", mj);
                    for (String userCode : userCodeList) {
                        user = new UserGroupService().getUserByUserCode(userCode);
                        if(user != null){
                            sendMsg2LanLend(actTaskID, userCode,  generatUrl(user,  "任务列表", "izerui") , content, content);
                        }else{
                            log.error("获取发送者或者接收者信息错误.");
                            return;
                        }
                    }
                } catch (Exception e) {
                    log.error(e.getMessage());
                }
            }
        } catch (Exception e) {
            log.error("向GOCom发送信息错误." , e);
        }
	}
	public void sendDestoryMsg(HttpServletRequest request, HttpServletResponse response) {
	    try {
	        String operUserCode = request.getParameter("operUserCode");
	        String actTaskId = request.getParameter("actTaskId");
	        if(StringUtils.isEmpty(operUserCode) || StringUtils.isEmpty(actTaskId)){
	            log.error("获取发送者或者接收者信息错误.");
	            return;
	        }else{
	            destoryMsg2LanLend(actTaskId, operUserCode);
	        }
	    } catch (Exception e) {
	        log.error("向GOCom发送信息错误." , e);
	    }
	}
	
	@Deprecated
	public void sendmsg4Lams(HttpServletRequest request, HttpServletResponse response) {
		try {
			String url = getServletContext().getInitParameter("GoComWebAdd");
			String goComMsgTitle = getServletContext().getInitParameter("GoComMsgTitle");
			String userCodes = request.getParameter("userCodes");
			String varsJson = request.getParameter("varsJson");

			String sqrbm = "";
			String sqrxm = "";
			String sqyy = "";
			String mj = "";
			SUser user = null;
			if(StringUtils.isNotEmpty(varsJson) && StringUtils.isNotEmpty(userCodes)){
				ObjectMapper mapper = new ObjectMapper();  
				Map<String,Object> vars = null;
				try {
					String[] userCodeList = StringUtils.split(userCodes,"[,]");
					vars = mapper.readValue(varsJson, Map.class); 
					sqrxm = (vars.get("sqrxm") == null ? "" : vars.get("sqrxm").toString());
					sqrbm = (vars.get("sqrbm") == null ? "" : vars.get("sqrbm").toString());
					sqyy = (vars.get("sqyy") == null ? "" : vars.get("sqyy").toString());
					mj = (vars.get("mj") == null ? "" : vars.get("mj").toString());
					String content = BaseDao.SEND_MSG_VM.replace("@usergroupname", sqrbm);
					content = content.replace("@username", sqrxm);
					content = content.replace("@sqyy", sqyy);
					content = content.replace("@mj", mj);
					for (String userCode : userCodeList) {
						UserGroupService ugs = new UserGroupService();
						user = ugs.getUserByUserCode(userCode);
						if(user != null){
							content = content.replace("@gotolamsurl", generatUrl(user,  "任务列表", "izerui"));
							System.out.println(content);
//							new FqspMessage(user.getUsercode(), user.getUsercode()
//									, content , goComMsgTitle, "" ,url ).sendMessage2GoCom();
							//发送消息
							//发送KK
						}else{
							log.error("获取发送者或者接收者信息错误.");
							return;
						}
					}
				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}
		} catch (Exception e) {
			log.error("向GOCom发送信息错误." , e);
		}
	}
	// 查看日志类表
	public void viewLogList(HttpServletRequest request, HttpServletResponse response) {
		try {
			Collection<File> listFile = FileUtils.listFiles(new File("c:/syncUserGroupLog"),  new   String[]{ "log","LOG"} , true);
			request.setAttribute("listFile", listFile);
			Utils.callJSP(request, response, "/page/listLog.jsp");
		} catch (Exception e) {
			log.error("获取日志列表错误." , e);
		}
	}
	// 查看具体某个日志
	public void viewLog(HttpServletRequest request, HttpServletResponse response) {
			PrintWriter out = null;
			try {
				response.setContentType( "text/html;charset=GBK "); 
				out = response.getWriter();
				out.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
				out.println("<HTML>");
				out.println("<BODY>");
				String filePath = request.getParameter("logFilePath");
				if(StringUtils.isEmpty(filePath)){
					out.print("获取日志错误!");
				}else{
					filePath = new String(filePath.getBytes("ISO-8859-1") , "UTF-8");
					File tempFile = null;
					Collection<File> listFile = FileUtils.listFiles(new File("c:/syncUserGroupLog"),  new   String[]{ "log","LOG"} , true);
					for (File file : listFile) {
						if(file.getName().equals(filePath)){
							tempFile = file;
							break;
						}
					}
					if(tempFile != null){
						List<String> stList = FileUtils.readLines(tempFile);
						for (String str : stList) {
							out.println(str+"<br/>");
						}
					}else{
						out.print("获取日志错误!");
					}
				}
				out.println("</BODY>");
				out.println("</HTML>");
			} catch (Exception e) {
				out.println("读取日志错误"+ e.getMessage());
				log.error("读取日志错误"+ e.getMessage());
			}finally{
				out.flush();
				out.close();
			}
	}

	protected void service(HttpServletRequest arg0, HttpServletResponse arg1)
			throws ServletException, IOException {
		dolist(arg0, arg1);
	}
	
	/**
	 * 得到登录Lams并且进入相应模块的URL
	 * @param user 用户
	 * @param modName 模块中文名称
	 * @param modOwner 模块所属者
	 */
	private String generatUrl(SUser user , String modName , String modOwner){
		StringBuffer sb = new StringBuffer();
		sb.append("http://").append(BaseDao.LAMS_IP).append("/Lams/autoLogin?card=").append(SeriKeyOper.encrypt(user.getUsercode()));
		sb.append("&serikey=").append(SeriKeyOper.encrypt(user.getPasswd())).append("&moduleName=");
		sb.append(SeriKeyOper.encrypt(modName)).append("&moduleOwner=").append(SeriKeyOper.encrypt(modOwner));
		sb.append("&random=").append(Math.random());
		return sb.toString();
	}

	private String cmdlist[] = { "listUser" , "listGroup" , "starSync" , "sendMsg" ,"viewLogList","viewLog","sendmsg4Lams","sendDestoryMsg"};
	private static final long serialVersionUID = 2621977516956958710L;
	private static Logger log = Logger.getLogger(SyncUserGroupServlet.class);
	
	
    private void sendMsg2LanLend(String taskid, String taskUserName, String taskURL, String title,
            String contenx) {
        NotifyTodoSendContext context = new NotifyTodoSendContext();
        context.setAppName("紫光档案管理系统");
        context.setModelName("赋权审批");
        context.setModelId(taskid);

        context.setSubject(title);
        context.setLink(taskURL);
        context.setType(1);
        context.setKey(contenx);
        context.setTargets("{\"LoginName\":\"" + taskUserName + "\"}");
        context.setCreateTime(com.thams.core.util.DateUtil.getCurrentTimeStr());
        try {
            NotifyTodoAppResult result =
                    getServer().getISysNotifyTodoWebServicePort().sendTodo(context);
            log.error(result.getReturnState());
            log.error(result.getMessage());
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void destoryMsg2LanLend(String taskid, String taskUserName) {
        NotifyTodoRemoveContext context = new NotifyTodoRemoveContext();
        context.setAppName("紫光档案管理系统");
        context.setModelName("赋权审批");
        context.setModelId(taskid);// 这是taskid
        context.setOptType(1);
        context.setTargets("{\"LoginName\":\"" + taskUserName + "\"}");
        try {
            NotifyTodoAppResult result = getServer().getISysNotifyTodoWebServicePort().deleteTodo(context);
            log.info(result.getReturnState());
            log.info(result.getMessage());
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public ISysNotifyTodoWebServiceService getServer() {
        ISysNotifyTodoWebServiceService ser = null;
        try {
            // http://192.168.1.90/sys/webservice/sysNotifyTodoWebService?wsdl 正式
            URL url = new URL(BaseDao.TODO_URL);
            QName qName = new QName("http://webservice.notify.sys.kmss.landray.com/",
                            "ISysNotifyTodoWebServiceService");
            ser = new ISysNotifyTodoWebServiceService(url, qName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ser;
    }
}
