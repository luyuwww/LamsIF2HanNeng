package com.thams.servlet;

import java.util.TimerTask;

import org.apache.log4j.Logger;

import com.thams.service.UserGroupService;

public class MyTask extends TimerTask {
	private Logger log =  Logger.getLogger(MyTask.class);
	public void run() {
		Thread thread = new Thread() {
			public void run() {
				UserGroupService us = new UserGroupService();
				try {
					log.error("定时器被调用");
					us.starSyncUserGroup();
				} catch (Exception e) {
					log.error("用户组同步数据错误"+ e.getMessage());
				}
			}
		};
		thread.setName("startSync");
		thread.start();
	}
}