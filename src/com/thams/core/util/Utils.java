package com.thams.core.util;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.thams.dao.BaseDao;
import com.thams.dao.SGroupMapper;
import com.thams.dao.SUserMapper;
import com.thams.entity.OAGroup;
import com.thams.entity.OAUser;
import com.thams.entity.SGroup;
import com.thams.entity.SUser;

public class Utils {
	/**
	 * 跳转页面使用
	 */
	public static void callJSP(HttpServletRequest request,
			HttpServletResponse response, String url) throws Exception {
		try {

			ServletContext sc = request.getSession(true).getServletContext();
			RequestDispatcher rd = sc.getRequestDispatcher(url);
			rd.forward(request, response);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * 打印消息
	 * 
	 * @param response
	 * @param JMessage
	 * @param isJs
	 *            是否是js
	 */
	public static void outPrint(HttpServletResponse response , String JMessage,
			boolean isJs) {
		response.setContentType("text/html;charset=UTF-8");
		try {
			PrintWriter out = response.getWriter();
			if (isJs == true) {// 是js脚本
				out.write("<script language='javascript'>");
				out.write(JMessage);
				out.write("</script>");
			} else {
				out.write(JMessage);
			}
			out.close();
		} catch (Exception e) {
			System.out
					.println("java:com.thams.WebArchive.common.action.commonAction.outPrint --Error :"
							+ e.getMessage());
		}
	}
	
	/**
	 * 将中间表的group对象装换成 Sgroup
	 */
	public static SGroup convertOaGroup2SGroup(OAGroup oaGroup){
		if(null != oaGroup){
			SGroup group = new SGroup();
			group.setDid(oaGroup.getDid());
			group.setQzh(oaGroup.getQzh());
			group.setGid(0);
			group.setPid(oaGroup.getPid());
			group.setGname(oaGroup.getGname());
			group.setBz(oaGroup.getBz());
			return group;
		}else{
			return null;
		}
	}
	/**
	 * 将中间表的OAUSER对象装换成 SUser
	 */
	public static SUser convertOaUser2Suser(OAUser oauser){
		if(null != oauser){
			SUser user = new SUser();
			user.setPid(oauser.getPid());
			user.setUsercode(oauser.getUsercode());
			user.setUsername(oauser.getUsername());
			user.setPasswd(oauser.getPasswd());
			user.setEmail(oauser.getEmail());
			return user;
		}else{
			return null;
		}
	}
	
	/**
	 * 判断s_user是否是ouList中
	 * @param ouList
	 * @param sUser
	 */
	public static boolean isContains(List<OAUser> ouList , SUser sUser){
		boolean result = false;
		for (OAUser oaUser : ouList) {
			if (oaUser.getUsercode().equals(sUser.getUsercode())) {
				result = true;
				break;
			}
		}
		return result;
	}
	/**
	 * 判断用户是否修改
	 */
	public static boolean userHasChange( SUser aUser , SUser bUser){
		if(aUser.getUsername().equals(bUser.getUsername()) && aUser.getEmail().equals(bUser.getEmail())
				&& aUser.getPid().equals(bUser.getPid()) && aUser.getPasswd().equals(bUser.getPasswd())){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * 通过user得到BMID
	 * @author: LuYu
	 */
	public String getBMIDByUser(String userName){
		StringBuffer bmid = new StringBuffer();
		try {
			List<SGroup> groupList = this.getGroupList(this.getPidByUserName(userName) , null);
			bmid.append(groupList.get(0).getQzh());
			for (SGroup group : groupList) {
				bmid.append("_").append(group.getDid());
			}
		} catch (Exception e) {
			bmid.append("");
			log.error("getUserByUserCode类在通过groupDID得到group的时候的时错误,在 getGroupByDid is " + e.getMessage());
		}
		return bmid.toString();
	}
	/**
	 * 通过部门did得到BMID
	 * @author: LuYu
	 */
	public String getBMIDByGroupDid(Integer groupDid){
		StringBuffer bmid = new StringBuffer();
		try {
			List<SGroup> groupList = this.getGroupList(groupDid , null);
			bmid.append(groupList.get(0).getQzh());
			for (SGroup group : groupList) {
				bmid.append("_").append(group.getDid());
			}
		} catch (Exception e) {
			bmid.append("");
			log.error("getUserByUserCode类在通过groupDID得到group的时候的时错误,在 getGroupByDid is " + e.getMessage());
		}
		return bmid.toString();
	}
	
	/**
	 * 通过组的得到一个groupList 从小到大 从最底层到最高层
	 * @param 组的did
	 */
	private List<SGroup> getGroupList(Integer groupDid , List<SGroup> groupList){
		SGroup tempGroup = this.getGroupByDid(groupDid);
		if(groupList == null){
			groupList = new ArrayList<SGroup>();
		}
		groupList.add(tempGroup);
		if(tempGroup.getPid() != 0){
			this.getGroupList(tempGroup.getPid(), groupList);
		}
		return groupList;
		
	}
	private SGroup getGroupByDid(Integer did){
		SGroup group = null;
		SqlSession session = null;
		SGroupMapper gMapper = null;
		try {
			session = BaseDao.getSqlSession();
			gMapper = session.getMapper(SGroupMapper.class);
			group = gMapper.selectByPrimaryKey(did);
		} catch (Exception e) {
			log.error(" getGroupByDid is " + e.getMessage());
		}finally{
			session.close();
		}
		return group;
	}
	private Integer getPidByUserName(String userName){
		Integer pid = -1;
		SqlSession session = null;
		SUserMapper uMapper = null;
		try {
			session = BaseDao.getSqlSession();
			uMapper = session.getMapper(SUserMapper.class);
			pid = uMapper.getPidByUserName(userName);
		} catch (Exception e) {
			log.error(" getPidByUserName is " + e.getMessage());
		}finally{
			session.close();
		}
		return pid;
	}
	private static Logger log = Logger.getLogger(Utils.class);
}
