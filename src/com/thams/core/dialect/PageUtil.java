package com.thams.core.dialect;

/**
 * @author: luyu
 * @date: Sep 22, 2008
 *  
 * @Function: 
 */
public class PageUtil {
	 public static final String SQL_END_DELIMITER = ";";  
	 public static final String SQL_ORDER_BY_METHOD_NAME = "getOrderByClause";  
	 public static final String SQL_PRIMARYKEY_METHOD_NAME = "getPrimaryKey";  
	 public static final String DEFAULT_PRIMARYKEY = "DID";  
	 public static final String DEFAULT_ORDERBY = "DID";  
	 public static final int DEFAULT_PAGE_SIZE = 10;
	 
	 
	 /**
	 * @author: luyu
	 * @data: Sep 9, 2008
	 * @param sql 语句
	 * @return  sql 语句
	 * @function: 去除sql语句最后的 ";" 号 和两边的空格
	 */
	public static String trim(String sql) {  
	        sql = sql.trim();  
	        if (sql.endsWith(SQL_END_DELIMITER)) {  
	            sql = sql.substring(0, sql.length() - 1  
	                    - SQL_END_DELIMITER.length());  
	        }  
	        return sql; //返回时将所有sql语句转换为大写 
	}
	
	public static enum E_DB_TYPE{
		MYSQL,
		ORACLE,
		SQLSERVER
	}
}
