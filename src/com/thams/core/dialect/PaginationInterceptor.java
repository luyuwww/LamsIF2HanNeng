package com.thams.core.dialect;

import java.sql.Connection;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.ibatis.executor.parameter.DefaultParameterHandler;
import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.RowBounds;

import com.thams.core.dialect.i.Dialect4Ibatis;
import com.thams.core.dialect.impl.MySqlDialect4Ibatis;
import com.thams.core.dialect.impl.OracleDialect4Ibatis;
import com.thams.core.dialect.impl.SQLServerDialect4Ibatis;

@Intercepts({ @Signature(type = StatementHandler.class, method = "prepare", args = { Connection.class }) })
public class PaginationInterceptor implements Interceptor {

	private final static Log log = LogFactory
			.getLog(PaginationInterceptor.class);

	@Override
	public Object intercept(Invocation invocation) throws Throwable {
		StatementHandler statementHandler = (StatementHandler) invocation.getTarget();
		BoundSql boundSql = statementHandler.getBoundSql();
		MetaObject metaStatementHandler = MetaObject.forObject(statementHandler);
		RowBounds rowBounds = (RowBounds) metaStatementHandler.getValue("delegate.rowBounds");
		if (rowBounds == null || rowBounds == RowBounds.DEFAULT) {
			return invocation.proceed();
		}
		Configuration configuration = (Configuration) metaStatementHandler
				.getValue("delegate.configuration");
		String primaykeyField = PageUtil.DEFAULT_PRIMARYKEY;
		String orderbyField = PageUtil.DEFAULT_ORDERBY;
		try {
			DefaultParameterHandler dHandler = (DefaultParameterHandler) metaStatementHandler
					.getValue("delegate.parameterHandler");
			primaykeyField = dHandler.getParameterObject().getClass().getMethod(PageUtil.SQL_PRIMARYKEY_METHOD_NAME,new Class[0])
					.invoke(dHandler.getParameterObject(), new Class[0]).toString();
			orderbyField = dHandler.getParameterObject().getClass().getMethod(PageUtil.SQL_ORDER_BY_METHOD_NAME,new Class[0])
					.invoke(dHandler.getParameterObject(), new Class[0]).toString();
		} catch (Exception e) {
			// ignore
		}
		Dialect4Ibatis dialect = null;
		String dirverClassName = invocation.getArgs()[0].toString();
		System.out.println(dirverClassName);
		if(dirverClassName.contains("jtds")){
			dialect = new SQLServerDialect4Ibatis();
		}else if(dirverClassName.contains("mysql")){
			dialect = new MySqlDialect4Ibatis();
		}else if(dirverClassName.contains("oracle")){
			dialect = new OracleDialect4Ibatis();
		}else if(dirverClassName.contains("db2")){
			
		}else{
			dialect = new SQLServerDialect4Ibatis();
		}
		String originalSql = (String) metaStatementHandler
				.getValue("delegate.boundSql.sql");
		metaStatementHandler.setValue("delegate.boundSql.sql", dialect
				.getLimitString(originalSql, rowBounds.getOffset(),
						rowBounds.getLimit() , primaykeyField , orderbyField));
		metaStatementHandler.setValue("delegate.rowBounds.offset",
				RowBounds.NO_ROW_OFFSET);
		metaStatementHandler.setValue("delegate.rowBounds.limit",
				RowBounds.NO_ROW_LIMIT);
		if (log.isDebugEnabled()) {
			log.debug("生成分页SQL : " + boundSql.getSql());
		}
		return invocation.proceed();
	}

	@Override
	public Object plugin(Object target) {
		return Plugin.wrap(target, this);
	}

	@Override
	public void setProperties(Properties properties) {
	}

}
