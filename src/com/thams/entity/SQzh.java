package com.thams.entity;

public class SQzh {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column S_QZH.DID
	 * @mbggenerated
	 */
	private Integer did;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column S_QZH.QZH
	 * @mbggenerated
	 */
	private String qzh;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column S_QZH.QZMC
	 * @mbggenerated
	 */
	private String qzmc;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column S_QZH.ISDEF
	 * @mbggenerated
	 */
	private Integer isdef;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column S_QZH.BZ
	 * @mbggenerated
	 */
	private String bz;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column S_QZH.DID
	 * @return  the value of S_QZH.DID
	 * @mbggenerated
	 */
	public Integer getDid() {
		return did;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column S_QZH.DID
	 * @param did  the value for S_QZH.DID
	 * @mbggenerated
	 */
	public void setDid(Integer did) {
		this.did = did;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column S_QZH.QZH
	 * @return  the value of S_QZH.QZH
	 * @mbggenerated
	 */
	public String getQzh() {
		return qzh;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column S_QZH.QZH
	 * @param qzh  the value for S_QZH.QZH
	 * @mbggenerated
	 */
	public void setQzh(String qzh) {
		this.qzh = qzh;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column S_QZH.QZMC
	 * @return  the value of S_QZH.QZMC
	 * @mbggenerated
	 */
	public String getQzmc() {
		return qzmc;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column S_QZH.QZMC
	 * @param qzmc  the value for S_QZH.QZMC
	 * @mbggenerated
	 */
	public void setQzmc(String qzmc) {
		this.qzmc = qzmc;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column S_QZH.ISDEF
	 * @return  the value of S_QZH.ISDEF
	 * @mbggenerated
	 */
	public Integer getIsdef() {
		return isdef;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column S_QZH.ISDEF
	 * @param isdef  the value for S_QZH.ISDEF
	 * @mbggenerated
	 */
	public void setIsdef(Integer isdef) {
		this.isdef = isdef;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column S_QZH.BZ
	 * @return  the value of S_QZH.BZ
	 * @mbggenerated
	 */
	public String getBz() {
		return bz;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column S_QZH.BZ
	 * @param bz  the value for S_QZH.BZ
	 * @mbggenerated
	 */
	public void setBz(String bz) {
		this.bz = bz;
	}
}