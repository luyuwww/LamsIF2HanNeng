package com.thams.service;

import java.io.File;
import java.util.List;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.thams.dao.BaseDao;
import com.thams.entity.EFile;
import com.thams.entity.OAEfile;
import com.thams.entity.OAGZLXDWqt3;
import com.thams.entity.QAHTWqt2;
import com.thams.entity.QAWqt1;
import com.thams.entity.QT1;
import com.thams.entity.QT2;
import com.thams.entity.QT3;
import com.thams.entity.Wqt1;
import com.thams.entity.Wqt2;
import com.thams.entity.Wqt3;

public class DataOperService {
	public void starAll(){
		starShouFaWen();
		starHT();
		starGZLXD();
	}
	
	private void starShouFaWen(){
		SqlSession session = null;
		List<QAWqt1> listQAWqt1 = null;
		List<OAEfile> oaEfileList = null;
		Wqt1 wqt1 = null;
		EFile efile = null;
		int maxdid=-1;
		int eMaxdid=-1;
		try {
			session = BaseDao.getSqlSession(true);
			listQAWqt1 = BaseDao.getAllListQaWqt1ListByStatus(0);//0是未接受.1是已经接收的 2 错误
			for (QAWqt1 qaWqt1 : listQAWqt1) {
				try {
					QT1 qt1 = (QT1)qaWqt1;
					maxdid = BaseDao.getMaxDid("W_QT1");
					wqt1 = new Wqt1(qt1, maxdid);
					wqt1.setBmid(BaseDao.getBMIDByHanergy(qaWqt1.getQcbm()));
					wqt1.setPid(-1);//档案系统自己使用.
					wqt1.setSfxygd("1");
					wqt1.setSfylj("0");
					wqt1.setAttached(0);
					oaEfileList = BaseDao.getOaEfileListByPid(qaWqt1.getDid());
					for (OAEfile oaEfile : oaEfileList) {
						wqt1.setAttached(1);
						efile = new EFile();//DID,PID,EFILENAME,TITLE,EXT,PZM,PATHNAME,EFILEDB,EFILEID,STATUS,ATTR,ATTREX,CREATOR,CREATETIME,MD5
						eMaxdid = BaseDao.getMaxDid("E_FILEQT1");
						efile.setDid(eMaxdid);
						efile.setPid(maxdid);
						efile.setEfilename(renameEfile(oaEfile.getEfilename().replace(BaseDao.OA_SFW_PATH, ""), BaseDao.OA_SFW_PATH, oaEfile.getExt()));
						efile.setPathname(BaseDao.OA_SFW_PATH);
						efile.setTitle(oaEfile.getTitle());
						efile.setExt(oaEfile.getExt());
						efile.setPzm(BaseDao.DEFAULT_PZM);//放到默认配置名称
						efile.setStatus(0);
						efile.setAttr(0);
						efile.setAttrex(0);
						efile.setTablename("E_FILEQT1");
						BaseDao.insertEfile(efile);
					}
					BaseDao.insertWQT1(wqt1);//插入WQT1
					qaWqt1.setStatus(1);
				} catch (Exception e) {
					qaWqt1.setStatus(2);
				}
				BaseDao.updateStatus("OA_DATA", qaWqt1.getDid(), qaWqt1.getStatus());//同步完毕更新状态
			}
		} catch (Exception e) {
			log.error("出现异常" , e);
		}finally{
			session.close();
		}
	}
	
	public static void main(String[] args) {
//		System.out.println(BaseDao.getUserNameByCode("测试试试"));
		DataOperService us = new DataOperService();
		us.starAll();
		
	}
	
	private void starHT(){
		SqlSession session = null;
		List<QAHTWqt2> listQAWqt2 = null;
		List<OAEfile> oaEfileList = null;
		Wqt2 wqt2 = null;
		EFile efile = null;
		int maxdid=-1;
		int eMaxdid=-1;
		try {
			session = BaseDao.getSqlSession(true);
			listQAWqt2 = BaseDao.getAllListQaWqt2ListByStatus(0);//0是未接受.1是已经接收的 2 错误
			for (QAHTWqt2 qaWqt2 : listQAWqt2) {
				try {
					QT2 qt2 = (QT2)qaWqt2;
					maxdid = BaseDao.getMaxDid("W_QT2");
					wqt2 = new Wqt2(qt2, maxdid);
					wqt2.setPid(-1);//档案系统自己使用.
					wqt2.setSfxygd("1");
					wqt2.setSfylj("0");
					wqt2.setAttached(0);//电子文件有是1 
					wqt2.setBmid(BaseDao.getBMIDByHanergy(qaWqt2.getQcbm()));
					oaEfileList = BaseDao.getOaEfileListByPid(qaWqt2.getDid());
					for (OAEfile oaEfile : oaEfileList) {
						wqt2.setAttached(1);
						efile = new EFile();//DID,PID,EFILENAME,TITLE,EXT,PZM,PATHNAME,EFILEDB,EFILEID,STATUS,ATTR,ATTREX,CREATOR,CREATETIME,MD5
						eMaxdid = BaseDao.getMaxDid("E_FILEQT2");
						efile.setDid(eMaxdid);
						efile.setPid(maxdid);
						efile.setEfilename(renameEfile(oaEfile.getEfilename().replace(BaseDao.OA_HT_PATH, ""), BaseDao.OA_HT_PATH, oaEfile.getExt()));
						efile.setPathname(BaseDao.OA_HT_PATH);
						efile.setTitle(oaEfile.getTitle());
						efile.setExt(oaEfile.getExt());
						efile.setPzm(BaseDao.DEFAULT_PZM);//放到默认配置名称
						efile.setStatus(0);
						efile.setAttr(0);
						efile.setAttrex(0);
						efile.setTablename("E_FILEQT2");
						BaseDao.insertEfile(efile);
					}
					BaseDao.insertWQT2(wqt2);//插入WQT1
					qaWqt2.setStatus(1);//1 完成
				} catch (Exception e) {
					qaWqt2.setStatus(2);//出现异常
				}
				BaseDao.updateStatus("OA_HT_DATA", qaWqt2.getDid(), qaWqt2.getStatus());//同步完毕更新状态
			}
		} catch (Exception e) {
			log.error("出现异常" , e);
		}finally{
			session.close();
		}
	}
	private void starGZLXD(){
		SqlSession session = null;
		List<OAGZLXDWqt3> listoAGzlxd = null;
		List<OAEfile> oaEfileList = null;
		Wqt3 wqt3 = null;
		EFile efile = null;
		int maxdid=-1;
		int eMaxdid=-1;
		try {
			session = BaseDao.getSqlSession(true);
			listoAGzlxd = BaseDao.getAllListQaGzLxListByStatus(0);
			for (OAGZLXDWqt3 qzlx : listoAGzlxd) {
				try {
					QT3 qt3 = (QT3)qzlx;
					maxdid = BaseDao.getMaxDid("W_QT3");
					wqt3 = new Wqt3(qt3, maxdid);
					wqt3.setPid(-1);//档案系统自己使用.
					wqt3.setSfxygd("1");
					wqt3.setSfylj("0");
					wqt3.setAttached(0);
					wqt3.setBmid(BaseDao.getBMIDByHanergy(qzlx.getQcbm()));
					oaEfileList = BaseDao.getOaEfileListByPid(qzlx.getDid());
					for (OAEfile oaEfile : oaEfileList) {
						wqt3.setAttached(1);
						efile = new EFile();//DID,PID,EFILENAME,TITLE,EXT,PZM,PATHNAME,EFILEDB,EFILEID,STATUS,ATTR,ATTREX,CREATOR,CREATETIME,MD5
						eMaxdid = BaseDao.getMaxDid("E_FILEQT3");
						efile.setDid(eMaxdid);
						efile.setPid(maxdid);
						efile.setEfilename(renameEfile(oaEfile.getEfilename().replace(BaseDao.OA_GZLXD_PATH, ""), BaseDao.OA_GZLXD_PATH, oaEfile.getExt()));
						efile.setPathname(BaseDao.OA_GZLXD_PATH);
						efile.setTitle(oaEfile.getTitle());
						efile.setExt(oaEfile.getExt());
						efile.setPzm(BaseDao.DEFAULT_PZM);//放到默认配置名称
						efile.setStatus(0);
						efile.setAttr(0);
						efile.setAttrex(0);
						efile.setTablename("E_FILEQT3");
						BaseDao.insertEfile(efile);
					}
					BaseDao.insertWQT3(wqt3);//插入WQT1
					qzlx.setStatus(1);
				} catch (Exception e) {
					qzlx.setStatus(2);
				}
				BaseDao.updateStatus("OA_GZLXD_DATA", qzlx.getDid(), qzlx.getStatus());//同步完毕更新状态
			}
		} catch (Exception e) {
			log.error("出现异常" , e);
		}finally{
			session.close();
		}
	}
	
	/**
	 * OA 传过来的是有汉字的文件.这里处理一下 copy 一份附件
	 * @param fileName 
	 * @param typePath 
	 * @param ext 扩展名
	 */
	private String renameEfile(String fileName , String typePath,String ext){
		try {
			String tempFile = UUID.randomUUID().toString()+System.currentTimeMillis()+"."+ext;
			File sf = new File(BaseDao.DEFAULT_PATH+typePath+fileName);
			File tf = new File(BaseDao.DEFAULT_PATH+typePath+tempFile);
			FileUtils.copyFile(sf , tf);
			return tempFile;
		} catch (Exception e) {
			e.printStackTrace();
			return fileName;
		}
	}
	
	private static Logger log = Logger.getLogger(DataOperService.class);
}
