package com.thams.service;

import java.util.List;

import org.apache.ibatis.exceptions.PersistenceException;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.thams.core.util.Utils;
import com.thams.dao.BaseDao;
import com.thams.dao.OAGroupMapper;
import com.thams.dao.OAUserMapper;
import com.thams.dao.SGroupMapper;
import com.thams.dao.SQzhMapper;
import com.thams.dao.SUserMapper;
import com.thams.entity.OAGroup;
import com.thams.entity.OAGroupExample;
import com.thams.entity.OAUser;
import com.thams.entity.OAUserExample;
import com.thams.entity.SGroup;
import com.thams.entity.SGroupExample;
import com.thams.entity.SQzh;
import com.thams.entity.SQzhExample;
import com.thams.entity.SUser;
import com.thams.entity.SUserExample;

public class UserGroupService {
	public void starSyncUserGroup(){
		starSyncGroup();
		starSyncUser();
	}
	
	private void starSyncGroup(){
		SqlSession session = null;
		SGroup wantToInsrtSGroup = null;
		String tempTableName = "TempGroupTable";
		dropTable(tempTableName);//先删除一下临时表.可能会抛出异常.但是已经捕获.
		SGroupMapper sGroupMapper = null;
		OAGroupMapper oaGroupMapper = null;
		SGroupExample sGroupExample = new SGroupExample();
		OAGroupExample oaGrouExample = new OAGroupExample();
		sGroupExample.createCriteria().andGnameIsNotNull();
		oaGrouExample.createCriteria().andGnameIsNotNull();
		try {
			session = BaseDao.getSqlSession(true);
			sGroupMapper = session.getMapper(SGroupMapper.class);
			oaGroupMapper = session.getMapper(OAGroupMapper.class);
			List<OAGroup> oaGroupList = oaGroupMapper.selectByExample(oaGrouExample);//得到OAgorup
			if(oaGroupList.size() < 20){//大致判断一下数据的合法性
				log.error("检查部门数量,发现数据太少,请检查数据是否正确");
				return;
			}
			sGroupMapper.copyTable("S_GROUP", tempTableName );//copy表 到TempGroupTable
			sGroupMapper.deleteByExample(sGroupExample);//删除s_group所有数据
			for (OAGroup oaGroup : oaGroupList) {//循环OAGROUP插入S_group
				wantToInsrtSGroup = Utils.convertOaGroup2SGroup(oaGroup);
				if(null != wantToInsrtSGroup){
					try {
						sGroupMapper.insert(wantToInsrtSGroup);
					} catch (Exception e) {
						log.error("插入一个部门发生错误" , e);
					}
				}
			}
		} catch (Exception e) {
			log.error("出现异常" , e);
		}finally{
			dropTable(tempTableName);//先删除一下临时表.可能会抛出异常.但是已经捕获.
			session.close();
		}
	}
	public static void main(String[] args) {
		UserGroupService us = new UserGroupService();
		us.starSyncUserGroup();
	}
	private void starSyncUser(){
		//GET ALL USER
		// CONTINAS USER
		// 存在update 不存在add
		SUser sUser = null;
		SqlSession session = null;
		SUserMapper sUserMapper = null;
		OAUserMapper oaUsermapper = null;
		SUserExample suEx = new SUserExample();
		OAUserExample oaUEx = new OAUserExample();
		oaUEx.createCriteria().andUsercodeIsNotNull().andUsercodeNotEqualTo("ROOT");
		suEx.createCriteria().andUsercodeNotEqualTo("ROOT");
		try {
			session = BaseDao.getSqlSession(true);
			sUserMapper = session.getMapper(SUserMapper.class);
			oaUsermapper = session.getMapper(OAUserMapper.class);
			List<SUser> sUserList = sUserMapper.selectByExample(suEx);
			List<OAUser> oaUserList = oaUsermapper.selectByExample(oaUEx);
			if(oaUserList.size() < 90){
				log.error("从中间库获取OA用户数太少,数据量:"+oaUserList.size()+".可能数据错误,取消同步.");
				return;
			}
			for (SUser suser : sUserList) {//oa中没有的用户删除
				if(!Utils.isContains(oaUserList, suser)){//不包含删除
					SUserExample desuserEx = new SUserExample();
					desuserEx.createCriteria().andUsercodeEqualTo(suser.getUsercode());
					sUserMapper.deleteByExample(desuserEx);
				}
			}
			for (OAUser oaUser : oaUserList) {//档案中没有的用户插入
				sUser = Utils.convertOaUser2Suser(oaUser);
				if(null != sUser){
					if(sUserList.contains(sUser)){
						if(!Utils.userHasChange(sUserList.get(sUserList.indexOf(sUser)), sUser)){//判断是否修改.修改了就UPdate
							sUserMapper.updateByUserCode(sUser);
							log.error("用户信息修改:"+sUser.toString());
						}
					}else{
						sUser.setDid(BaseDao.getMaxDid("S_USER"));
						try {
							sUserMapper.insertNoBlob(sUser);
						} catch (Exception e) {
							log.error("出现异常"+e.getMessage());
						}
						log.error("增加了一个用户:"+sUser.toString());
					}
				}
			}
		} catch (Exception e) {
			log.error("出现异常"+e.getMessage());
		}finally{
			session.close();
		}
	}
	/**
	 * 得到所有usercode不为空的
	 */
	public List<SUser> getAllUser(){
		SUserExample user = new SUserExample();
		user.createCriteria().andUsercodeIsNotNull();
		SUserMapper uMapper = BaseDao.getSqlSession().getMapper(SUserMapper.class);
		return uMapper.selectByExample(user);
	}
	/**
	 * 得到所有qzh不为空的
	 */
	public List<SQzh> getAllQzh(){
		SQzhExample qzh = new SQzhExample();
		qzh.createCriteria().andQzhIsNotNull();
		SQzhMapper qzhMapper = BaseDao.getSqlSession().getMapper(SQzhMapper.class);
		return qzhMapper.selectByExample(qzh);
	}
	
	/**
	 * 得到所有group不为空的
	 */
	public List<SGroup> getAllGroup(){
		SGroupExample group = new SGroupExample();
		group.createCriteria().andGnameIsNotNull();
		SGroupMapper groupMapper = BaseDao.getSqlSession().getMapper(SGroupMapper.class);
		return groupMapper.selectByExample(group);
	}
	/**
	 * 根据用户的userEmail得到用户
	 */
	public SUser getUserByEmail(String email){
		SUserExample userExample = new SUserExample();
		userExample.createCriteria().andEmailEqualTo(email);
		SUserMapper userMapper = BaseDao.getSqlSession().getMapper(SUserMapper.class);
		List<SUser> resultList = userMapper.selectByExample(userExample);
		if(resultList.size()>0){
			return resultList.get(0);
		}else{
			return null;
		}
	}
	/**
	 * 根据用户的userEmail得到用户
	 */
	public SUser getUserByUserCode(String userCode){
		return BaseDao.getSqlSession().getMapper(SUserMapper.class).getUserByCode(userCode);
	}
	
	private void dropTable(String tableName){
		SqlSession session = null;
		SGroupMapper gm = null;
		try {
			session = BaseDao.getSqlSession(true);
			gm = session.getMapper(SGroupMapper.class);
			gm.dropTable(tableName);
		} catch (PersistenceException e) {
			log.error("删除表为空."+e.getMessage());
		}finally{
			session.close();
		}
	}
	
	private static Logger log = Logger.getLogger(UserGroupService.class);
	
}
