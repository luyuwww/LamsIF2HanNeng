package net.wantong;

import java.rmi.RemoteException;

import cn.nowhelp.bean.GoComWebService;
import cn.nowhelp.bean.WebService;

/**
 * 万通临时复权模块 发信息
 * 
 * @author: LUYUWWW
 */
public class FqspMessage {
	public String url = null;

	private String sender;
	private String receiver;
	private String bContent;
	private String title;
	private String expand;
	WebService ser = null;

	public void sendMessage2GoCom() {
		WebService ser = new WebService();// zhangsan,wangwu,liyi,cese,lisi
		GoComWebService service = ser.WebServiceClient(url);
		try {
			service.sendOAMessage(sender, receiver, bContent, title, expand);
		} catch (RemoteException e) {
			System.out.println("消息没有发送: " + e.getMessage());
		}
	}

	public FqspMessage(String sender, String receiver, String content , String title, String expand,String url) {
		super();
		ser = new WebService();
		this.sender = sender;
		this.receiver = receiver;
		bContent = content;
		this.title = title;
		this.expand = expand;
		this.url = url;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getBContent() {
		return bContent;
	}

	public void setBContent(String content) {
		bContent = content;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getExpand() {
		return expand;
	}

	public void setExpand(String expand) {
		this.expand = expand;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public FqspMessage() {
	}

}
