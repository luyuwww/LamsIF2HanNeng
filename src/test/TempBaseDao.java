package test;

import java.net.URL;

import javax.xml.namespace.QName;

import com.landray.kmss.sys.notify.webservice.ISysNotifyTodoWebServiceService;
import com.landray.kmss.sys.notify.webservice.NotifyTodoAppResult;
import com.landray.kmss.sys.notify.webservice.NotifyTodoRemoveContext;
import com.landray.kmss.sys.notify.webservice.NotifyTodoSendContext;

public class TempBaseDao {//saaj-impl-1.3.2.jar  需要这个jar
	public static void main(String[] args) throws Exception {
		TempBaseDao tbd = new TempBaseDao();
//		tbd.sendMsg2LanLend("0-121212-2sdf51s5df1", "wangna01",
//				"http://www.baidu.com", "西班牙语", "这是是测试消息");
		tbd.destoryMsg2LanLend("0-121212-2sdf51s5df1", "wangxing");
	}

	private void sendMsg2LanLend(String taskid, String taskUserName,
			String taskURL, String title, String contenx) {
		NotifyTodoSendContext context = new NotifyTodoSendContext();
		context.setAppName("紫光档案管理系统");
		context.setModelName("赋权审批");
		context.setModelId(taskid);

		context.setSubject(title);
		context.setLink(taskURL);
		context.setType(1);
		context.setKey(contenx);
		context.setTargets("{\"LoginName\":\"" + taskUserName + "\"}");
		context.setCreateTime(com.thams.core.util.DateUtil.getCurrentTimeStr());
		try {
			NotifyTodoAppResult result = getServer()
					.getISysNotifyTodoWebServicePort().sendTodo(context);
			System.out.println(result.getReturnState());
			System.out.println(result.getMessage());
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
	 private void destoryMsg2LanLend(String taskid, String taskUserName) {
	        NotifyTodoRemoveContext context = new NotifyTodoRemoveContext();
	        context.setAppName("紫光档案管理系统");
	        context.setModelName("赋权审批");
	        context.setModelId(taskid);// 这是taskid
	        context.setOptType(1);
	        context.setTargets("{\"LoginName\":\"" + taskUserName + "\"}");
	        try {
	            NotifyTodoAppResult result = getServer().getISysNotifyTodoWebServicePort().deleteTodo(context);
	            System.out.println(result.getReturnState());
	            System.out.println(result.getMessage());
	        } catch (Exception e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }

	    }

	public ISysNotifyTodoWebServiceService getServer() {
		ISysNotifyTodoWebServiceService ser = null;
		try {
			// http://192.168.1.90/sys/webservice/sysNotifyTodoWebService?wsdl
			// 正式
			// URL url = new
			// URL("http://192.168.1.90/sys/webservice/sysNotifyTodoWebService?wsdl");
			URL url = new URL(
					"http://192.168.1.90/sys/webservice/sysNotifyTodoWebService?wsdl");
			// URL url = new
			// URL("http://192.168.19.80/sys/webservice/sysNotifyTodoWebService?wsdl");
			QName qName = new QName(
					"http://webservice.notify.sys.kmss.landray.com/",
					"ISysNotifyTodoWebServiceService");
			ser = new ISysNotifyTodoWebServiceService(url, qName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ser;
	}
}
