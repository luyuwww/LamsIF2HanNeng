<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<html>
  	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>列出所有日志</title>
	    <link href="${pageContext.request.contextPath}/js/bootstrap-3.0.3-dist/css/bootstrap.css" rel="stylesheet">
	    <link href="${pageContext.request.contextPath}/js/bootstrap-3.0.3-dist/css/bootstrap-theme.min.css" rel="stylesheet">
	    <link href="${pageContext.request.contextPath}/js/self/theme.css" rel="stylesheet">
	</head>
<body>
	<c:forEach var="file" items="${listFile}" varStatus="count">
		 ${count.index}. ${file.name}<a
		  class="btn btn-lg btn-link" href="${pageContext.request.contextPath}/SyncUserGroup?op=viewLog&logFilePath=${file.name}&rand=<%=Math.random()%>"> 查看</a><br>
	</c:forEach>
	<script src="${pageContext.request.contextPath}/js/jquery1.10.2/jquery-1.10.2.min.js" type="text/javascript"></script> 
	<script src="${pageContext.request.contextPath}/js/bootstrap-3.0.3-dist/js/bootstrap.min.js" type="text/javascript"></script> 
</body>
</html>
