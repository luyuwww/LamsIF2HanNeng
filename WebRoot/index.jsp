<%@page import="java.util.Random"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
  	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Lams同步工具</title>
	    <link href="${pageContext.request.contextPath}/js/bootstrap-3.0.3-dist/css/bootstrap.css" rel="stylesheet">
	    <link href="${pageContext.request.contextPath}/js/bootstrap-3.0.3-dist/css/bootstrap-theme.min.css" rel="stylesheet">
	    <link href="${pageContext.request.contextPath}/js/self/theme.css" rel="stylesheet">
	</head>
  	<body>
		<div class="container theme-showcase">
			<p>
				   <a class="btn btn-lg btn-primary"
			   			href="${pageContext.request.contextPath}/SyncUserGroup?op=listUser&rand=<%=Math.random()%>">列出所有用户</a>
				   <a class="btn btn-lg btn-success"
				   		href="${pageContext.request.contextPath}/SyncUserGroup?op=listGroup&rand=<%=Math.random()%>">列出所有部门</a>
				   <a class="btn btn-lg btn-info"
				   		href="${pageContext.request.contextPath}/SyncUserGroup?op=viewLogList&rand=<%=Math.random()%>">查看日志</a>
					<br>
				   <button class="btn btn-lg btn-warning"
				   		onclick="starUserGroupSync()">开始用户部门同步</button>
				   <button class="btn btn-lg btn-danger"
				   		onclick="starDataSync()">开始数据同步</button>
			</p>
		</div>
		<!--  
		<button onclick="send2GoCom()">向goCom发送信息</button>
		-->
	<script src="${pageContext.request.contextPath}/js/jquery1.10.2/jquery-1.10.2.min.js" type="text/javascript"></script> 
	<script src="${pageContext.request.contextPath}/js/bootstrap-3.0.3-dist/js/bootstrap.min.js" type="text/javascript"></script> 
  	</body>
  <script type="text/javascript">
  function starUserGroupSync(){
	  $.post("${pageContext.request.contextPath}/SyncUserGroup?op=starSync&rand=<%=Math.random()%>",{},
				function (data, textStatus){
		  			alert(data);
				}, "text");
  }
  function starDataSync(){
	  $.post("${pageContext.request.contextPath}/SyncData?op=starSync&rand=<%=Math.random()%>",{},
				function (data, textStatus){
		  			alert(data);
				}, "text");
  }
  function send2GoCom(){
	  $.post("${pageContext.request.contextPath}/SyncUserGroup?op=sendmsg&subject=111&textContextext=sssss&fromEmail=hantong@hanergy&target=hantong@hanergy&rand=<%=Math.random()%>",{},
				function (data, textStatus){
		  			//alert(data);
				}, "text");
  }
  </script>
</html>
